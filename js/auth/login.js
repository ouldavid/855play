$(document).ready(function() {
    // register social active
    $('ul.social-register li').click(function() {
        $('ul.social-register li').removeClass('active');
        $(this).closest('ul.social-register li').addClass('active');
    });
    // country select
    $(".input-country, .input-social-country").countrySelect({
        defaultCountry: "kh",
        defaultStyling: "inside",
        responsiveDropdown: true
    });
    // intlTelInput
    var input_group_phone = document.querySelector(".input-phone");
    window.intlTelInput(input_group_phone, {
        // any initialisation options go here
        preferredCountries: ["kh","us","gb"],
    });
    var input_group_mail = document.querySelector(".input-mail-phone");
    window.intlTelInput(input_group_mail, {
        // any initialisation options go here
        preferredCountries: ["kh","us","gb"],
    });

});

