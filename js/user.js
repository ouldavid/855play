$(document).ready(function() {
    //
});
// bootstrap modal
/*function modal_deposit_aba() {
    $.ajax({
        type: 'GET',
        url: "ajax/deposit-aba.html",
        dataType: "html",
        mimeType: "text/html; charset=utf-8",
        success : function (data) {
            $("#modal-deposit .modal-content").html(data);
        }
    });
}
function modal_deposit_aceleda() {
    $.ajax({
        type: 'GET',
        url : "/user/deposit-aceleda",
        success : function (data) {
            $("#modal-deposit .modal-content").html(data);
        }
    });
}
function modal_deposit_pipay() {
    $.ajax({
        type: 'GET',
        url : "/user/deposit-pipay",
        success : function (data) {
            $("#modal-deposit .modal-content").html(data);
        }
    });
}
function modal_deposit_truemoney() {
    $.ajax({
        type: 'GET',
        url : "/user/deposit-truemoney",
        success : function (data) {
            $("#modal-deposit .modal-content").html(data);
        }
    });
}
function modal_deposit_wing() {
    $.ajax({
        type: 'GET',
        url : "/user/deposit-wing",
        success : function (data) {
            $("#modal-deposit .modal-content").html(data);
        }
    });
}*/
// withdraw
/*function modal_withdraw_aba() {
    $.ajax({
        type: 'GET',
        url : "/user/withdraw-aba",
        success : function (data) {
            $("#modal-withdraw .modal-content").html(data);
        }
    });
}
function modal_withdraw_aceleda() {
    $.ajax({
        type: 'GET',
        url : "/user/withdraw-aceleda",
        success : function (data) {
            $("#modal-withdraw .modal-content").html(data);
        }
    });
}
function modal_withdraw_pipay() {
    $.ajax({
        type: 'GET',
        url : "/user/withdraw-pipay",
        success : function (data) {
            $("#modal-withdraw .modal-content").html(data);
        }
    });
}
function modal_withdraw_truemoney() {
    $.ajax({
        type: 'GET',
        url : "/user/withdraw-truemoney",
        success : function (data) {
            $("#modal-withdraw .modal-content").html(data);
        }
    });
}
function modal_withdraw_wing() {
    $.ajax({
        type: 'GET',
        url : "/user/withdraw-wing",
        success : function (data) {
            $("#modal-withdraw .modal-content").html(data);
        }
    });
}*/
// bootstrap datepicker
$(function () {
    //Date range as a button
    $('#daterangepicker span').html(moment().format('D MMM') + ' - ' + moment().format('D MMM'));
    $('#daterangepicker').daterangepicker(
        {
            ranges   : {
                'Today'       : [moment(), moment()],
                'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment(),
            endDate  : moment(),
            maxDate: moment(),
            opens: "right"
        },
        function (start, end) {
            $('#daterangepicker span').html(start.format('D MMM') + ' - ' + end.format('D MMM'))
        }
    )
})
// responsive tabs
! function($) {
    "use strict";
    var a = {
        accordionOn: ["xs"]
    };
    $.fn.responsiveTabs = function(e) {
        var t = $.extend({}, a, e),
            s = "";
        return $.each(t.accordionOn, function(a, e) {
            s += " accordion-" + e
        }), this.each(function() {
            var a = $(this),
                e = a.find("> li > a"),
                t = $(e.first().attr("href")).parent(".tab-content"),
                i = t.children(".tab-pane");
            a.add(t).wrapAll('<div class="responsive-tabs-container" />');
            var n = a.parent(".responsive-tabs-container");
            n.addClass(s), e.each(function(a) {
                var t = $(this),
                    s = t.attr("href"),
                    i = "",
                    n = "",
                    r = "";
                t.parent("li").hasClass("active") && (i = " active"), 0 === a && (n = " first"), a === e.length - 1 && (r = " last"), t.clone(!1).addClass("accordion-link" + i + n + r).insertBefore(s)
            });
            var r = t.children(".accordion-link");
            e.on("click", function(a) {
                a.preventDefault();
                var e = $(this),
                    s = e.parent("li"),
                    n = s.siblings("li"),
                    c = e.attr("href"),
                    l = t.children('a[href="' + c + '"]');
                s.hasClass("active") || (s.addClass("active"), n.removeClass("active"), i.removeClass("active"), $(c).addClass("active"), r.removeClass("active"), l.addClass("active"))
            }), r.on("click", function(t) {
                t.preventDefault();
                var s = $(this),
                    n = s.attr("href"),
                    c = a.find('li > a[href="' + n + '"]').parent("li");
                s.hasClass("active") || (r.removeClass("active"), s.addClass("active"), i.removeClass("active"), $(n).addClass("active"), e.parent("li").removeClass("active"), c.addClass("active"))
            })
        })
    }
}(jQuery);

// responsive tabs
$('.responsive-tabs').responsiveTabs({
    accordionOn: ['xs', 'sm']
});
