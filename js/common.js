$(document).ready(function() {
    // active menu
    var url = window.location;
    $('.navbar-nav a[href="'+ url +'"]').parent().addClass('active');
    $('.navbar-nav a').filter(function() {
        return this.href == url;
    }).parent().addClass('active');
    // active dropdown menu
    $('.navbar-nav .dropdown a[href="'+ url +'"]').parents('li.dropdown').addClass('active');
    $('.navbar-nav .dropdown a').filter(function() {
        return this.href == url;
    }).parents('li.dropdown').addClass('active');

    // footer accordion
    $(".footer-accordion[data-toggle='collapse']").removeAttr("data-toggle");
    if($(window).width() < 767.98)
    {
        $(".footer-accordion").attr("data-toggle","collapse");
        $('footer .collapse').collapse("hide");
    }

});

// Navigation Header
window.onload=function() {
    if(window.jQuery) {
        $(document).ready(function() {
            $(".sidebarNavigation .navbar-collapse").hide().clone().appendTo("body").removeAttr("class").addClass("sideMenu").show();
            $("body").append("<div class='overlay'></div>");
            $(".navbar-toggle, .navbar-toggler").on("click",function() {
                $(".sideMenu").addClass($(".sidebarNavigation").attr("data-sidebarClass"));
                $(".sideMenu, .overlay").toggleClass("open");
                $(".overlay").on("click",function(){$(this).removeClass("open");$(".sideMenu").removeClass("open")})
            });
            $("body").on("click",".sideMenu.open .nav-item",function() {
                if(!$(this).hasClass("dropdown")){$(".sideMenu, .overlay").toggleClass("open")}
            });
            $(window).resize(function() {
                if($(".navbar-toggler").is(":hidden")){$(".sideMenu, .overlay").hide()}else{$(".sideMenu, .overlay").show()}
            })
        })
    }else{console.log("sidebarNavigation Requires jQuery")}
}

// reload table
function reload_page() {
	window.location.href=window.location.href;
}
