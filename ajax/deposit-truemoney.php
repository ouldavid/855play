<div class="modal-header">
    <h5 class="modal-title m-auto" id="depositTitle"><img src="{{ asset('img/cards/true_money.jpg') }}" class="img-fluid" alt="True Money"></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form method="POST" action="" role="form" id="frm-truemoney">
    {{ csrf_field() }}
    <div class="modal-body">
        <div class="box-body">
            <div class="form-group required has-feedback{{ $errors->has('amount') ? ' has-error' : '' }}">
                <label class="control-label text-uppercase" for="input-amount">Amount (Min. 1 USD)</label>
                <input type="number" class="form-control" id="input-amount" name="amount" placeholder="1 USD" min="1" required>
                @if ($errors->has('amount'))
                    <span class="help-block">
                        <strong>{{ $errors->first('amount') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group required has-feedback{{ $errors->has('account') ? ' has-error' : '' }}">
                <label class="control-label text-uppercase" for="input-account">True Money Account</label>
                <input type="tel" class="form-control" id="input-account" name="account" placeholder="" value="(855) 10-xxx-xx63" required>
                @if ($errors->has('account'))
                    <span class="help-block">
                        <strong>{{ $errors->first('account') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group required has-feedback{{ $errors->has('account') ? ' has-error' : '' }}">
                <label class="control-label text-uppercase" for="input-name">Account Name</label>
                <input type="text" class="form-control" id="input-name" name="name" placeholder="" value="Full Name" required>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group required has-feedback{{ $errors->has('account') ? ' has-error' : '' }}">
                <label class="control-label text-uppercase" for="input-transactionID">Transaction ID</label>
                <input type="number" class="form-control" id="input-transactionID" name="transaction_id" placeholder="" required>
                @if ($errors->has('transaction_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('transaction_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="modal-footer">
        <div class="mb-3 w-100">
            <p class="text-danger font-weight-bold">Before requesting a deposit, please make a transfer using the payment details stated below.</p>
        </div>
        <div class="mb-3 w-100">
            <p class="font-weight-bold">Transfer To: <span>855Acc (000 000 000)</span></p>
        </div>
        <button type="submit" class="btn btn-primary mx-auto bg-gradient border-0 w-50">Confirm</button>
    </div>
</form>
