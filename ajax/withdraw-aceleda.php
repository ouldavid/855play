<div class="modal-header">
    <h5 class="modal-title m-auto" id="withdrawTitle"><img src="{{ asset('img/cards/acleda.jpg') }}" class="img-fluid" alt="ACELEDA Bank"></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form method="POST" action="" role="form" id="frm-acleda">
    {{ csrf_field() }}
    <div class="modal-body">
        <div class="box-body">
            <div class="form-group required has-feedback{{ $errors->has('amount') ? ' has-error' : '' }}">
                <label class="control-label text-uppercase" for="input-amount">Amount (Min. 10 USD)</label>
                <input type="number" class="form-control" id="input-amount" name="amount" placeholder="10 USD" min="10" required>
                @if ($errors->has('amount'))
                    <span class="help-block">
                        <strong>{{ $errors->first('amount') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group required has-feedback{{ $errors->has('account') ? ' has-error' : '' }}">
                <label class="control-label text-uppercase" for="input-account">ACELEDA Account</label>
                <input type="number" class="form-control" id="input-account" name="account" placeholder="xxxx xxxx xxxx xxxx" required>
                @if ($errors->has('account'))
                    <span class="help-block">
                        <strong>{{ $errors->first('account') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group required has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="control-label text-uppercase" for="input-name">Account Name</label>
                <input type="text" class="form-control" id="input-name" name="name" placeholder="Your account name" required>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary mx-auto bg-gradient border-0 w-50">Confirm</button>
    </div>
</form>
