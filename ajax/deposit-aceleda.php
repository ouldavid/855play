<div class="modal-header">
    <h5 class="modal-title m-auto" id="depositTitle"><img src="{{ asset('img/cards/acleda.jpg') }}" class="img-fluid" alt="ACELEDA Bank"></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form method="POST" action="" role="form" id="frm-aceleda">
    {{ csrf_field() }}
    <div class="modal-body">
        <div class="box-body">
            <div class="form-group required has-feedback{{ $errors->has('amount') ? ' has-error' : '' }}">
                <label class="control-label text-uppercase" for="input-amount">Amount (Min. 1 USD)</label>
                <input type="number" class="form-control" id="input-amount" name="amount" placeholder="1 USD" min="1" required>
                @if ($errors->has('amount'))
                    <span class="help-block">
                        <strong>{{ $errors->first('amount') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group required has-feedback{{ $errors->has('account') ? ' has-error' : '' }}">
                <label class="control-label text-uppercase" for="input-account">ACELEDA Account</label>
                <input type="number" class="form-control" id="input-account" name="account" placeholder="xxxx xxxx xxxx xxxx" required>
                @if ($errors->has('account'))
                    <span class="help-block">
                        <strong>{{ $errors->first('account') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="modal-footer">
        <div class="mb-3 w-100">
            <p class="text-danger font-weight-bold">Before requesting a deposit, please make a transfer using the payment details stated below.</p>
        </div>
        <div class="mb-3 w-100">
            <p class="font-weight-bold">Transfer To: <span>855Acc (000 000 000)</span></p>
        </div>
        <button type="submit" class="btn btn-primary mx-auto bg-gradient border-0 w-50">Confirm</button>
    </div>
</form>
