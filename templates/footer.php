<div class="container">
    <div class="row" id="footer-partner">
        <div class="col-12 col-md-6 d-flex justify-content-center mb-3">
            <ul class="text-center">
                <h5 class="text-white text-uppercase mb-3">OFFICIAL PARTNERS</h5>
                <li>
                    <a href="#"><img src="img/brands/ppc-fc.png" alt="PPCFC" /></a>
                </li>
                <li>
                    <a href="#"><img src="img/brands/pi-pay.png" alt="Pi Pay" /></a>
                </li>
            </ul>
        </div>
        <div class="col-12 col-md-6 d-flex justify-content-center">
            <ul class="text-center">
                <h5 class="text-white text-uppercase mb-3">CORPORATE PARTNERS</h5>
                <li>
                    <a href="#"><img src="img/brands/855Crown.png" alt="855Crown" /></a>
                </li>
                <li>
                    <a href="#"><img src="img/brands/ASKAP.png" alt="ASKAP Gold" /></a>
                </li>
                <li>
                    <a href="#"><img src="img/brands/GC855.png" alt="GC855" /></a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid bg-black">
    <div class="container">
        <div class="row" id="footer-info">
            <div class="col-md-3 mb-3 d-flex justify-content-center">
                <ul class="accordion-block">
                    <h5 class="text-white text-uppercase footer-accordion" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">INFORMATION</h5>
                    <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Registration</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Affiliates</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Responsible Gaming</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Security</a>
                        </li>
                    </div>
                </ul>
            </div>
            <div class="col-md-3 mb-3 d-flex justify-content-center">
                <ul class="accordion-block">
                    <h5 class="text-white text-uppercase footer-accordion" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">PRODUCTS</h5>
                    <div id="collapseTwo" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Sports Betting</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Online Casino</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Live Casino</a>
                        </li>
                    </div>
                </ul>
            </div>
            <div class="col-md-3 mb-3 d-flex justify-content-center">
                <ul class="accordion-block">
                    <h5 class="text-white text-uppercase footer-accordion" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseOne">INFO CENTRE</h5>
                    <div id="collapseThree" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Promotions</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Help Centre</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Payment Methods</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Contact Us</a>
                        </li>
                    </div>
                </ul>
            </div>
            <div class="col-md-3 mb-3 d-flex justify-content-center">
                <ul class="accordion-block">
                    <h5 class="text-white text-uppercase footer-accordion" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseOne">BETTING</h5>
                    <div id="collapseFour" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Sports Results</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Betting Statistics</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Sports Betting</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Casino Betting</a>
                        </li>
                    </div>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid bg-black border-t-gray">
    <div class="container">
    <div class="row" id="footer-nav">
        <ul class="col-12 text-center">
            <li class="mb-2">
                <a href="#">About us</a>
            </li>
            <li class="mb-2">
                <a href="#">Contact us</a>
            </li>
            <li class="mb-2">
                <a href="#">Terms & Conditions</a>
            </li>
            <li class="mb-2">
                <a href="#">Privacy Policy</a>
            </li>
            <li class="mb-2">
                <a href="#">Game rule</a>
            </li>
            <li class="mb-2">
                <a href="#">Disclaimer</a>
            </li>
        </ul>
    </div>
</div>
</div>
<div class="container-fluid bg-black border-t-gray">
    <div class="container" id="footer-bt">
        <div class="row">
            <div class="col-lg-4 justify-content-center justify-content-lg-start mb-3">
                <div id="paymentMethod" class="text-center text-lg-left">
                    <h5 class="text-white text-uppercase mb-3">Payment Method</h5>
                    <div class="icon-container">
                        <i class="icon-img"><img src="img/cards/small-visa.png" alt="Visa Card" /></i>
                        <i class="icon-img"><img src="img/cards/small-master.png" alt="Master Card" /></i>
                        <i class="icon-img"><img src="img/cards/small-pipay.png" alt="Pi Pay" /></i>
                        <i class="icon-img"><img src="img/cards/small-true.png" alt="True Money" /></i>
                        <i class="icon-img"><img src="img/cards/small-wing.png" alt="Wing" /></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex justify-content-center mb-3">
                <div id="social-qr">
                    <h5 class="text-center text-white text-uppercase mb-3">Connect via Social Media</h5>
                    <ul class="popup-qrcode">
                        <li>
                            <a href="#" data-toggle="modal" data-target="#modal-QR-telegram"><i class="fab fa-telegram telegram" alt="Telegram"></i> Telegram</a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#modal-QR-line"><i class="fab fa-line line" alt="Line"></i> Line</a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#modal-QR-whatsapp"><i class="fab fa-whatsapp-square whatsapp" alt="WhatsApp"></i> WhatsApp</a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#modal-QR-skype"><i class="fab fa-skype skype" alt="Skype"></i> Skype</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 d-flex justify-content-center justify-content-lg-end mb-3">
                <div id="email-subscribe" class="text-center text-lg-right">
                    <h5 class="text-white text-uppercase mb-3">Email Subscriber</h5>
                    <form class="form-subscribe">
                        <div class="input-group">
                            <input type="email" class="form-control" name="email" placeholder="Email address">
                            <span class="input-group-btn">
                                <button class="btn btn-default text-uppercase" type="submit">subscribe</button>
                           </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <p class="text-center text-white">&copy; Copyright 855PLAY <span class="text-primary">2020</span>, All Rights reserve. | 18+</p>
    </div>
</div>


<!-- social network -->
<div class="social-fix">
    <nav class="social">
        <ul class="social-media">
            <li><a href="#"><i class="fab fa-facebook-f facebook"></i> Facebook</a></li>
            <li><a href="#"><i class="fab fa-youtube youtube"></i> Youtube</a></li>
        </ul>
        <ul class="social-network">
            <li class="position-relative">
                <div class="btn-network">
                    <i class="fas fa-headset"></i>
                    <span class="text-uppercase">Support 24/7</span>
                </div>
                <a href="#"><i class="fab fa-telegram telegram"></i> Telegram</a>
                <a href="#"><i class="fab fa-skype skype"></i> Skype</a>
                <a href="#"><i class="fab fa-whatsapp-square whatsapp"></i> Whatsapp</a>
                <a href="#"><i class="fab fa-weixin weixin"></i> Wechat</a>
                <a href="#" class="border-0 pb-0"><i class="fas fa-phone-alt phone"></i> +088 544 4855</a>
                <a href="#" class="multi">+010 875 855</a>
            </li>
        </ul>
    </nav>
</div>

<!-- Modal Social QR -->
<div class="modal fade" id="modal-QR-telegram" tabindex="-1" role="dialog" aria-labelledby="telegramTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="box-body">
                    <img class="img-fluid m-auto" src="img/social-media/qr-telegram.png" alt="Telegram" />
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-QR-line" tabindex="-1" role="dialog" aria-labelledby="lineTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="box-body">
                    <img class="img-fluid m-auto" src="img/social-media/qr-line.png" alt="Line" />
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-QR-whatsapp" tabindex="-1" role="dialog" aria-labelledby="whatsappTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="box-body">
                    <img class="img-fluid m-auto" src="img/social-media/qr-whatsapp.png" alt="Telegram" />
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-QR-skype" tabindex="-1" role="dialog" aria-labelledby="skypeTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="box-body">
                    <img class="img-fluid m-auto" src="img/social-media/qr-skype.png" alt="Telegram" />
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</div>