<nav class="navbar navbar-expand-xl navbar-dark bg-black sidebarNavigation" data-sidebarClass="navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="/"><img class="img-fluid" src="img/logo/logo.gif" alt="855Play"></a>
        <button class="navbar-toggler leftNavbarToggler" type="button" data-toggle="" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="nav navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a class="nav-link" href="/"><i class="fas fa-home"></i> Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="dropdown mega-dropdown nav-item"> <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">SPORTS</a>
                    <ul class="dropdown-menu mega-dropdown-menu">
                        <li class="dropdown-item">
                            <div class="row">
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="sport.html" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/all-sports.png" alt="All 855sport" />
                                        </a>
                                        <div class="card-body">
                                            <a href="sport.html" class="text-uppercase">All SPORT</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/sports-855sport.png" alt="855sport" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase">855 SPORT</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/sports-betting.png" alt="Sports Betting" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase">Sports Betting</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/sports-play-sbo.png" alt="SBO Sport" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase">SBO Sport</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li><!-- sport dropdown -->
                <li class="dropdown mega-dropdown nav-item"> <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">CASINO</a>
                    <ul class="dropdown-menu mega-dropdown-menu">
                        <li class="dropdown-item">
                            <div class="row">
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="casino.html" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/all-casino.png" alt="All Casino" />
                                        </a>
                                        <div class="card-body">
                                            <a href="casino.html" class="text-uppercase">All Casino</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/casino-dg.png" alt="DG Casino" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase"><img class="img-fluid pr-2" src="img/brands/dg-casino.png" alt="DG Casino" />DGCASINO</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/casino-w88.png" alt="W88 Casino" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase"><img class="img-fluid pr-2" src="img/brands/w88-casino.png" alt="W88 Casino" />w88</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/casino-asia.png" alt="Asia Casino" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase"><img class="img-fluid pr-2" src="img/brands/asia-casino.png" alt="Asia Casino" />Asia Casino</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li><!-- casino dropdown -->
                <li class="dropdown mega-dropdown nav-item"> <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">KENO & LOTTERY</a>
                    <ul class="dropdown-menu mega-dropdown-menu">
                        <li class="dropdown-item">
                            <div class="row">
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="keno-lottery.html" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/all-keno.png" alt="KENO & LOTTERY" />
                                        </a>
                                        <div class="card-body">
                                            <a href="keno-lottery.html" class="text-uppercase">KENO & LOTTERY</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/keno-play.png" alt="Play Keno" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase">Play Keno</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/keno-play-2D3D.png" alt="Play 2D3D" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase">Play 2D3D</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/keno-fish.png" alt="Keno Fish" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase">Fish</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li><!-- keno & lottery -->
                <li class="dropdown mega-dropdown nav-item"> <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">GAME</a>
                    <ul class="dropdown-menu mega-dropdown-menu">
                        <li class="dropdown-item">
                            <div class="row">
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="game.html" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/all-game.png" alt="All Game" />
                                        </a>
                                        <div class="card-body">
                                            <a href="game.html" class="text-uppercase">All Game</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/game-855.png" alt="855 Game" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase">855 Game</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/game-jili.png" alt="Jili Game" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase">Jili Game</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-center px-0">
                                    <div class="card">
                                        <a href="#" class="thumbnail">
                                            <img class="img-fluid" src="img/nav/game-w88.png" alt="W88 Game" />
                                        </a>
                                        <div class="card-body">
                                            <a href="#" class="text-uppercase">W88 Game</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li><!-- game -->               
                <li class="nav-item">
                    <a class="nav-link" href="promo.html">PROMO</a>
                </li>
                <!-- language -->
                <li class="nav-item d-xl-none">
                    <a class="nav-link d-inline-block pr-2" href="#"><span class="flag-icon flag-icon-gb-eng"></span> ENG</a>
                    <a class="nav-link d-inline-block pr-2" href="#"><span class="flag-icon flag-icon-kh"></span> KH</a>
                    <a class="nav-link d-inline-block pr-2" href="#"><span class="flag-icon flag-icon-vn"></span> VN</a>
                </li>
            </ul>
        </div>

        <div id="nav-right">     
			<!-- no login -->
			<ul class="no-login">
				<li class="d-none d-md-block border-0 px-0">
					<div class="btn-over">
						<a href="register.html" class="btn register">Join Now!</a>
					</div>
				</li>
				<li class="d-none d-md-block"><a href="#" class="btn sign-in"><i class="fas fa-user"></i> login</a></li>
				<!-- mobile size -->
				<li class="nav-item dropdown d-md-none border-0">
					<a class="nav-link dropdown-toggle px-0 py-1" href="#" id="dropdown-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fas fa-user rounded-circle"></span></a>
					<div class="dropdown-menu" aria-labelledby="dropdown-user">
						<a class="dropdown-item" href="register.html">Join Now</a>
						<a class="dropdown-item" href="#">login</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle px-0 py-1" href="#" id="dropdown-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="flag-icon flag-icon-gb-eng"> </span></a>
					<div class="dropdown-menu" aria-labelledby="dropdown-language">
						<a class="dropdown-item" href="#"><span class="flag-icon flag-icon-kh"></span></a>
						<a class="dropdown-item" href="#"><span class="flag-icon flag-icon-vn"></span></a>
					</div>
				</li>
			</ul>        
        </div>
    </div>
</nav>